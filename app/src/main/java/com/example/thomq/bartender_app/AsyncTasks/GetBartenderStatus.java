package com.example.thomq.bartender_app.AsyncTasks;

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.BartenderStatus;
import com.example.thomq.bartender_app.Services.DataService;

public class GetBartenderStatus extends AsyncTask<AsyncModel, Void, BartenderStatus> {
    private AsyncModel model;
    @Override
    protected BartenderStatus doInBackground(AsyncModel... models) {
        model = models[0];
        return DataService.getInstance().getBartenderStatus();
    }

    @Override
    protected void onPostExecute(BartenderStatus bartenderStatus) {
        if(!DataService.getInstance().getBartenderStatus().isMakingDrink()){

        }else {
            if(DataService.getInstance().getBartenderStatus().getNumberOfDrinksToMake() == 0)
                Toast.makeText(model.getContext(),
                        "The Bartender is making a drink.", Toast.LENGTH_LONG).show();
            else Toast.makeText(model.getContext(),
                    "The Bartender need to make " + Integer.toString(
                            DataService.getInstance().getBartenderStatus().getNumberOfDrinksToMake() + 1
                    ) + " drinks", Toast.LENGTH_LONG).show();
        }
    }
}
