package com.example.thomq.bartender_app.ArrayAdapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.thomq.bartender_app.Models.Ingredient;

import java.util.List;

public class IngredientArrayAdapter extends ArrayAdapter<Ingredient> {
    private Context context;
    private List<Ingredient> ingredients;
    public IngredientArrayAdapter(@NonNull Context context, int resource,
                                  @NonNull List<Ingredient> objects) {
        super(context, resource, objects);
        this.context = context;
        this.ingredients = objects;
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Nullable
    @Override
    public Ingredient getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(ingredients.get(position).getDisplayName());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(ingredients.get(position).getDisplayName());
        return label;
    }
}
