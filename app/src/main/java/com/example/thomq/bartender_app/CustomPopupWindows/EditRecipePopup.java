package com.example.thomq.bartender_app.CustomPopupWindows;

import android.content.Context;
import android.graphics.Color;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.thomq.bartender_app.AsyncTasks.SaveRecipeTask;
import com.example.thomq.bartender_app.CustomWidgets.Recipes.RecipeInfo;
import com.example.thomq.bartender_app.Models.Recipe;

public class EditRecipePopup extends CustomPopWindowBase {
    public EditRecipePopup(Context context, Recipe recipe) {
        super(context);

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setBackgroundColor(Color.WHITE);

        RecipeInfo recipeInfo = new RecipeInfo(context, recipe);
        layout.addView(recipeInfo);


        LinearLayout btnLayout = new LinearLayout(context);
        btnLayout.setOrientation(LinearLayout.HORIZONTAL);

        Button saveBtn = new Button(context);
        saveBtn.setText("Save");
        saveBtn.setOnClickListener(v -> {
            Recipe recipe1 = recipeInfo.getRecipe();
            if(recipe1 != null) new SaveRecipeTask(recipe1).execute();
        });

        btnLayout.addView(saveBtn);



        layout.addView(btnLayout);
        setContentView(layout);
        setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
    }
}
