package com.example.thomq.bartender_app.CustomWidgets.Recipes;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.example.thomq.bartender_app.AsyncTasks.SetRecipeRatingTask;
import com.example.thomq.bartender_app.CustomWidgets.ComponentRow;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class RecipeInfo extends ScrollView {
    private Context context;
    private Recipe recipe;

    EditText recipeNameTxt;
    RatingBar recipeRatingBar;
    TableLayout ingredientTable;

    public RecipeInfo(Context context) {
        super(context);
        setup(context);
    }

    public RecipeInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context);
    }

    public RecipeInfo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup(context);
    }

    public RecipeInfo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setup(context);
    }

    public RecipeInfo(Context context, Recipe recipe) {
        this(context);
        setup(recipe);
    }

    public RecipeInfo(Context context, AttributeSet attrs, Recipe recipe) {
        this(context, attrs);
        setup(recipe);
    }

    public RecipeInfo(Context context, AttributeSet attrs, int defStyleAttr, Recipe recipe) {
        this(context, attrs, defStyleAttr);
        setup(recipe);
    }

    public RecipeInfo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, Recipe recipe) {
        this(context, attrs, defStyleAttr, defStyleRes);
        setup(recipe);
    }


    private void setup(Context context){
        this.context = context;
        LinearLayout layout = (LinearLayout) ((LayoutInflater) Objects.requireNonNull(context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)))
                .inflate(R.layout.recipe_information_layout, null);

        recipeNameTxt = layout.findViewById(R.id.recipeInformationName);
        recipeRatingBar = layout.findViewById(R.id.recipeInformationRatingBar);
        ingredientTable = layout.findViewById(R.id.recipeInformationIngredientTable);

        recipeRatingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            if(fromUser){
                new SetRecipeRatingTask(recipe, rating).execute();
            }
        });

        addView(layout);
    }

    private void setup(Recipe recipe){
        this.recipe = recipe;
        recipeNameTxt.setText(recipe.getName());
        recipeRatingBar.setRating((float) recipe.getRating());
        recipe.getIngredients().forEach((ingredient, amount)->{
            ingredientTable.addView(new ComponentRow(context, ingredientTable, amount, ingredient));
        });
    }

    private boolean saveRecipe(){
        if(recipe == null) recipe = new Recipe();
        if(recipeNameTxt.getText() == null || recipeNameTxt.getText().toString().isEmpty()){
            Toast.makeText(context, "Your Recipe needs a name.", Toast.LENGTH_LONG).show();
            return false;
        }else if(ingredientTable.getChildCount() == 0){
            Toast.makeText(context, "You recipe needs ingredients.", Toast.LENGTH_LONG).show();
            return false;
        }else {
            recipe.setName(recipeNameTxt.getText().toString());

            double totalAmount = 0;
            double volume = 0;
            List<ComponentRow> componentRows = new ArrayList<>();
            for(int index=0; index < ingredientTable.getChildCount(); index++) {
                ComponentRow row = (ComponentRow) ingredientTable.getChildAt(index);
                totalAmount += row.getAmount();
                volume += row.getAmount() * row.getIngredient().getDensity();
                componentRows.add(row);
            }

            recipe.setServingSize(totalAmount);
            Map<Ingredient, Double> ingredients = new HashMap<>();
            double finalTotalAmount = totalAmount;
            componentRows.forEach(row->ingredients.put(row.getIngredient(), row.getAmount() / finalTotalAmount));
            recipe.setIngredients(ingredients);
            return true;
        }
    }


    public Recipe getRecipe() {
        return saveRecipe() ? recipe:null;
    }

    public TableLayout getIngredientTable() {
        return ingredientTable;
    }
}
