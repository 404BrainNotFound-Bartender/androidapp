package com.example.thomq.bartender_app.Views.Ingredients;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.example.thomq.bartender_app.AsyncTasks.CreateIngredientTask;
import com.example.thomq.bartender_app.CustomWidgets.Ingredients.IngredientInfo;
import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.R;

public class AddIngredient extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ingredient);
        IngredientInfo ingredientInfo = findViewById(R.id.addIngredientIngredientInfo);
        Button saveBtn = findViewById(R.id.addIngredientSaveBtn);
        Button saveTrainBtn = findViewById(R.id.addIngredientSaveTrainBtn);
        Button clearBtn = findViewById(R.id.addIngredientClearBtn);


        saveBtn.setOnClickListener(v -> {
            CreateIngredientTask task = new CreateIngredientTask();
            Ingredient ingredient = ingredientInfo.getIngredient();
            if(ingredient != null) task.execute(new AsyncModel(this, ingredient, ingredientInfo));
        });
    }
}
