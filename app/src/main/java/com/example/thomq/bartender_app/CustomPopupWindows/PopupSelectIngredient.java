package com.example.thomq.bartender_app.CustomPopupWindows;

import android.app.SearchManager;
import android.content.Context;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ListView;

import com.example.thomq.bartender_app.ArrayAdapters.IngredientArrayAdapter;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.R;
import com.example.thomq.bartender_app.Services.DataService;

import java.util.Locale;

public class PopupSelectIngredient extends CustomPopWindowBase implements SearchView.OnQueryTextListener {
    private Ingredient ingredient;
    private SearchView searchView;
    private ListView listView;
    private Button button;
    private SearchManager searchManager;
    private IngredientArrayAdapter arrayAdapter;

    public PopupSelectIngredient(Context context) {
        super(context, R.layout.popup_select_ingredient);
        this.searchView = getContentView().findViewById(R.id.ingredientSelectSearchView);
        this.listView = getContentView().findViewById(R.id.ingredientSelectListView);
        this.button = getContentView().findViewById(R.id.ingredientSelectCreateIngredient);

        arrayAdapter = new IngredientArrayAdapter(context,
                android.R.layout.list_content, DataService.getInstance().getIngredients());

        listView.setAdapter(arrayAdapter);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        String lower = s.toLowerCase();
        arrayAdapter.clear();
        if(lower.isEmpty()) arrayAdapter.addAll(DataService.getInstance().getIngredients());
        else {
            DataService.getInstance().getIngredients().forEach(ingredient1 -> {
                if(ingredient1.getName().toLowerCase(Locale.getDefault()).contains(lower)){
                    arrayAdapter.add(ingredient1);
                }
            });
        }
        return false;
    }
}
