package com.example.thomq.bartender_app.Views.Recipes;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TableLayout;

import com.example.thomq.bartender_app.AsyncTasks.AddRecipeTask;
import com.example.thomq.bartender_app.CustomWidgets.ComponentRow;
import com.example.thomq.bartender_app.CustomWidgets.Recipes.RecipeInfo;
import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.R;

public class AddRecipe extends AppCompatActivity {

    private TableLayout table;
    private RecipeInfo recipeInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        FloatingActionButton addBtn = findViewById(R.id.addComponent);
        Button createButton = findViewById(R.id.createRecipe);

        recipeInfo = findViewById(R.id.addRecipeInfo);
        table = recipeInfo.getIngredientTable();
        table.addView(new ComponentRow(AddRecipe.this, table));

        addBtn.setOnClickListener(v -> table.addView(new ComponentRow(AddRecipe.this, table)));

        createButton.setOnClickListener(view->{
            Recipe recipe = recipeInfo.getRecipe();
            if(recipe != null){
                AddRecipeTask task = new AddRecipeTask(AddRecipe.this, recipe);
                task.execute();
            }
        });
    }
}
