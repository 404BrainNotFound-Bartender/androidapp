package com.example.thomq.bartender_app.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class RestfulUtil{
    private static RestfulUtil instance;
    public static final String GET_MAKEABLE = "getMakeableRecipes/";

//    public static String SERVER_IP = "192.168.0.127";
    public static String SERVER_IP = "";
    public static final String ADD_INGREDIENT = "addIngredient/";
    public static final String GET_INGREDIENTS = "getIngredients/";
    public static final String ADD_RECIPE = "addRecipe/";
    public static final String GET_RECIPES = "getRecipes/";
    public static final String SAVE_RECIPE = "saveRecipe/";
    public static final String GET_IMAGE = "getImage/";
    public static final String SAVE_INGREDIENT = "saveIngredient/";
    public static final String GET_PUMPS = "getPumps/";
    public static final String ADD_PUMP = "addPump/";
    public static final String SAVE_PUMP = "savePump/";
    public static final String MAKE_RECIPE = "makeRecipe/";
    public static final String DRINK_STATUS = "drinkStatus/";
    public static final String GET_BARTENDER_STATUS = "getBartenderStatus/";
    public static final String ENTER_MANUAL_MODE = "enterManualMode/";
    public static final String DISPENSE_INGREDIENT = "dispenseIngredient/";
    public static final String LEAVE_MANUAL_MODE = "leaveManualMode/";
    public static final String TRIAN_INGREDIENT = "trainIngredientPID/";
    public static final String DELETE_RECIPE = "deleteRecipe/";
    public static final String TEST_CONNECT = "testConnection/";
    public static final String RATE_RECIPE = "rateRecipe/";
    public static final int PORT_NUMBER = 8000;

    private Future findServer;

    public static RestfulUtil getInstance(){
        if (instance == null) instance = new RestfulUtil();
        if(instance.findServer != null){
            try {
                instance.findServer.get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private RestfulUtil(){
       findServer = ThreadManager.getInstances().submit(this::lookForServer);
    }

    private String buildUrl(String url){
        return "http://" + SERVER_IP + ":8000" +
                "/bartender/" + url;
    }

    public JSONObject getData(String url){
        InputStream data = null;
        HttpURLConnection cc = null;
        try {
            URL urlCon  = new URL(buildUrl(url));
            cc = (HttpURLConnection) urlCon.openConnection();
            cc.setChunkedStreamingMode(0);
            cc.setRequestMethod("GET");
            cc.setReadTimeout(1000* 60);
            cc.connect();

            if(cc.getResponseCode() == HttpURLConnection.HTTP_OK) data = cc.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(data == null) {
            return null;
        }
        else {
            String json = ConvertStreamToString(data);
            try {
                data.close();
                cc.disconnect();
                return new JSONObject(json);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public void postData(String url, JSONObject payload){
        HttpURLConnection cc;
        try {
            URL urlCon  = new URL(buildUrl(url));
            cc = (HttpURLConnection) urlCon.openConnection();
            cc.setRequestMethod("POST");
            cc.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            cc.setDoOutput(true);


            DataOutputStream os = new DataOutputStream(cc.getOutputStream());
            os.writeBytes(payload.toString());
            os.flush();
            os.close();
            cc.connect();
            System.out.print(cc.getResponseMessage());

            cc.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject postGetData(String url, JSONObject payload){
        InputStream data = null;
        HttpURLConnection cc = null;
        try {
            URL urlCon  = new URL(buildUrl(url));
            cc = (HttpURLConnection) urlCon.openConnection();
            cc.setRequestMethod("POST");
            cc.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            cc.setDoOutput(true);


            DataOutputStream os = new DataOutputStream(cc.getOutputStream());
            os.writeBytes(payload.toString());
            os.flush();
            os.close();
            cc.connect();
            System.out.print(cc.getResponseMessage());
            if(cc.getResponseCode() == HttpURLConnection.HTTP_OK) data = cc.getInputStream();
//            cc.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(data == null) {
            if(cc != null) cc.disconnect();
            return null;
        }
        else {
            String json = ConvertStreamToString(data);
            try {
                data.close();
                cc.disconnect();
                return new JSONObject(json);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private String ConvertStreamToString(InputStream stream) {

        InputStreamReader isr = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder response = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    private void lookForServer(){
        String ip = null;
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress("google.com", 80));
            ip = socket.getLocalAddress().toString();
        }catch (IOException ignored){}
        if(ip != null) {
            String searchIp = ip.substring(1, ip.lastIndexOf(".") + 1);
            for (int i = 0; i < 255; i++) {
                Socket socket = new Socket();
                try {
                    socket.bind(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    socket.connect(new InetSocketAddress(searchIp + Integer.toString(i), PORT_NUMBER), 10);
                    SERVER_IP = searchIp + Integer.toString(i);
                    socket.close();
                    return;
                } catch (IOException ignored) {
                }
            }
        }
    }
}


