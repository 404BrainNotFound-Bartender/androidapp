package com.example.thomq.bartender_app.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;

import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;
import com.example.thomq.bartender_app.Views.Ingredients.AddIngredient;
import com.example.thomq.bartender_app.Views.Ingredients.ViewIngredients;
import com.example.thomq.bartender_app.Views.MainActivity;
import com.example.thomq.bartender_app.Views.Recipes.AddRecipe;

import org.json.JSONObject;

public class CreateIngredientTask extends AsyncTask<AsyncModel, Void, Void> {
    private AsyncModel model;
    private ProgressDialog dialog;
    @Override
    protected Void doInBackground(AsyncModel... asyncModels) {
        model = asyncModels[0];
        dialog = new ProgressDialog(model.getContext());
        dialog.setMessage("Saving Ingredient");
        dialog.show();

        JSONObject object = RestfulUtil.getInstance().postGetData(RestfulUtil.ADD_INGREDIENT, model.getModel().toJson());
        if(object != null)DataService.getInstance().getIngredients().add(new Ingredient(object));

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        dialog.dismiss();
        PopupWindow popupWindow = new PopupWindow(model.getContext());

        LinearLayout mainLayout = new LinearLayout(model.getContext());
        mainLayout.setOrientation(LinearLayout.VERTICAL);

        Button anotherIngredientBtn = new Button(model.getContext());
        Button viewIngredientsBtn  = new Button(model.getContext());
        Button makeRecipeBtn  = new Button(model.getContext());
        Button mainBtn = new Button(model.getContext());

        anotherIngredientBtn.setText("Create another ingredient");
        anotherIngredientBtn.setOnClickListener(v -> model.getContext().startActivity(new Intent(model.getContext(), AddIngredient.class)));

        viewIngredientsBtn.setText("View Ingredient");
        viewIngredientsBtn.setOnClickListener(v -> model.getContext().startActivity(new Intent(model.getContext(), ViewIngredients.class)));

        makeRecipeBtn.setText("Create Recipe");
        makeRecipeBtn.setOnClickListener(v -> model.getContext().startActivity(new Intent(model.getContext(), AddRecipe.class)));

        mainBtn.setText("To Main Page");
        mainBtn.setOnClickListener(v -> model.getContext().startActivity(new Intent(model.getContext(), MainActivity.class)));

        mainLayout.addView(anotherIngredientBtn);
        mainLayout.addView(viewIngredientsBtn);
        mainLayout.addView(makeRecipeBtn);
        mainLayout.addView(mainBtn);

        popupWindow.setContentView(mainLayout);
        popupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
        popupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.showAtLocation(model.getPopupAnchor(), Gravity.CENTER, 0, 0);
    }

}
