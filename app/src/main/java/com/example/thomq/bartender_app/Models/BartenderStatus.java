package com.example.thomq.bartender_app.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class BartenderStatus implements ModelBase {
    private String id;
    private boolean manualMode;
    private boolean makingDrink;
    private Integer numberOfDrinksToMake;

    public BartenderStatus(){}

    public BartenderStatus(JSONObject object){
        try {
            manualMode = object.getBoolean("manualMode");
            makingDrink = object.getBoolean("makingDrink");
            numberOfDrinksToMake = object.getInt("numberDrinksToMake");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public JSONObject toJson() {
        return null;
    }

    public boolean isManualMode() {
        return manualMode;
    }

    public boolean isMakingDrink() {
        return makingDrink;
    }

    public Integer getNumberOfDrinksToMake() {
        return numberOfDrinksToMake;
    }
}
