package com.example.thomq.bartender_app.CustomWidgets.Ingredients;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.thomq.bartender_app.ArrayAdapters.IngredientArrayAdapter;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.Services.DataService;

public class IngredientFilterRow extends TableRow {
    private Ingredient selectedIngredient;
    public IngredientFilterRow(Context context, TableLayout tableLayout) {
        super(context);

        Spinner ingredientSpinner = new Spinner(context);
        ingredientSpinner.setAdapter(new IngredientArrayAdapter(context,
                android.R.layout.simple_spinner_dropdown_item, DataService.getInstance().getIngredients()));
        ingredientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedIngredient = (Ingredient) ingredientSpinner.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        addView(ingredientSpinner);

        FloatingActionButton removeBtn = new FloatingActionButton(context);
        removeBtn.setImageResource(android.R.drawable.ic_delete);
        addView(removeBtn);
        removeBtn.setOnClickListener(v -> tableLayout.removeView(IngredientFilterRow.this));
    }

    public Ingredient getIngredient() {
        return selectedIngredient;
    }
}
