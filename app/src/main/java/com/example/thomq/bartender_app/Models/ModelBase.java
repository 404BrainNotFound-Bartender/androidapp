package com.example.thomq.bartender_app.Models;

import org.json.JSONObject;

public interface ModelBase {
    String getId();
    JSONObject toJson();
}
