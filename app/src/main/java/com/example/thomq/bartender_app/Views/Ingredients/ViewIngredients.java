package com.example.thomq.bartender_app.Views.Ingredients;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.example.thomq.bartender_app.AsyncTasks.LoadViewIngredientTask;
import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.R;

public class ViewIngredients extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_ingredients);
        Toolbar toolbar = findViewById(R.id.ingredientViewToolBar);
        setSupportActionBar(toolbar);


        NestedScrollView scrollView = findViewById(R.id.layout);
        LinearLayout linearLayout = findViewById(R.id.card_view);
        FloatingActionButton addBtn = findViewById(R.id.viewIngredientAdd);
        addBtn.setOnClickListener(view->{
            Intent intent = new Intent(ViewIngredients.this, AddIngredient.class);
            startActivity(intent);
        });

//        ProgressDialogPopup dialogPopup = new ProgressDialogPopup(this, "Fetching Ingredients");
        LoadViewIngredientTask task = new LoadViewIngredientTask();
//        dialogPopup.show(scrollView);
        task.execute(new AsyncModel(this, linearLayout));
    }
}
