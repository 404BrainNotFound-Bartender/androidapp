package com.example.thomq.bartender_app.AsyncTasks;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.MakeRequest;
import com.example.thomq.bartender_app.Services.NotificationHelper;
import com.example.thomq.bartender_app.Services.RestfulUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class MakeDrink extends AsyncTask<AsyncModel, Integer, Boolean> {
    private MakeRequest request;
    @SuppressLint("StaticFieldLeak")
    private NotificationHelper notificationHelper;
    @Override
    protected Boolean doInBackground(AsyncModel... asyncModels) {
        boolean stillMaking;
        boolean beingMade = false;
        boolean lastBeingMadeVale;
        AsyncModel asyncModel = asyncModels[0];
        request = (MakeRequest) asyncModel.getModel();
        notificationHelper = new NotificationHelper(asyncModel.getContext());
        do{
            JSONObject object = RestfulUtil.getInstance().getData(
                    RestfulUtil.DRINK_STATUS + request.getId() + "/"
            );
            try {
                stillMaking = !object.getBoolean("made");
                if(!stillMaking) break;
                lastBeingMadeVale = beingMade;
                beingMade = object.getBoolean("status");
                if(!lastBeingMadeVale && beingMade){
                    notificationHelper.notify(456,
                            notificationHelper.getDrinkStatusNotification(
                                    "Drink Status",
                                    "Your " + request.getRecipe().getName() +
                                            " Is Being Made!!!!"
                            )
                    );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while (true);
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean){
        notificationHelper.notify(90,
                notificationHelper.getDrinkStatusNotification(
                        "Drink Status",
                        "Your " + request.getRecipe().getName() + " Has Been Made!!!!"
                )
        );
    }
}
