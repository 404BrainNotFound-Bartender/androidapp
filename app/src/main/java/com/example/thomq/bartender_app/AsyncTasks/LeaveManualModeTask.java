package com.example.thomq.bartender_app.AsyncTasks;

import android.os.AsyncTask;

import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;

public class LeaveManualModeTask extends AsyncTask<Void, Void, Void> {
    @Override
    protected Void doInBackground(Void... voids) {
        RestfulUtil.getInstance().getData(RestfulUtil.LEAVE_MANUAL_MODE + DataService.getInstance().getManualModeId() + "/");
        return null;
    }
}
