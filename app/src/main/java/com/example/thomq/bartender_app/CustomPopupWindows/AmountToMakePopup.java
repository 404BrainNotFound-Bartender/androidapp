package com.example.thomq.bartender_app.CustomPopupWindows;

import android.content.Context;

import com.example.thomq.bartender_app.R;

public class AmountToMakePopup extends CustomPopWindowBase {
    AmountToMakePopup(Context context){
        super(context, R.layout.popup_amount_to_make);
    }
}
