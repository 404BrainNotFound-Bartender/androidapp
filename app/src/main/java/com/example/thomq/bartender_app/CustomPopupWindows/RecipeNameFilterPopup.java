package com.example.thomq.bartender_app.CustomPopupWindows;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.thomq.bartender_app.CustomWidgets.Recipes.RecipeCard;
import com.example.thomq.bartender_app.R;

import java.util.ArrayList;
import java.util.List;

public class RecipeNameFilterPopup extends CustomPopWindowBase {
    private ViewGroup recipeArea;
    private EditText nameTxt;

    public RecipeNameFilterPopup(Context context, ViewGroup  recipeArea) {
        super(context, R.layout.recipe_name_filter_popup);
        this.recipeArea = recipeArea;

        nameTxt = getContentView().findViewById(R.id.recipeNameFilterEditTxt);
        Button button = getContentView().findViewById(R.id.recipeNameFilterApplyBtn);
        button.setOnClickListener(this::handleApply);
    }


    private boolean handleApply(View view){
        String name = nameTxt.getText().toString().toLowerCase();
        List<RecipeCard> toRemove = new ArrayList<>();

        for(int i =0; i < recipeArea.getChildCount(); i++){
            RecipeCard card = (RecipeCard) recipeArea.getChildAt(i);
            if(!card.getRecipe().getName().toLowerCase().startsWith(name))toRemove.add(card);
        }

        toRemove.forEach(recipeArea::removeView);
        dismiss();
        return false;
    }
}
