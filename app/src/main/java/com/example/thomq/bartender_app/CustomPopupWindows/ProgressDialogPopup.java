package com.example.thomq.bartender_app.CustomPopupWindows;

import android.content.Context;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.thomq.bartender_app.R;

public class ProgressDialogPopup extends CustomPopWindowBase {
    ProgressBar progressBar;
    TextView messageTxt;
    public ProgressDialogPopup(Context context){
        super(context, R.layout.popup_progress_dialog);
        progressBar = getContentView().findViewById(R.id.progressDialogBar);
        messageTxt = getContentView().findViewById(R.id.progressDialogTxt);
    }

    public ProgressDialogPopup(Context context, String message){
        this(context);
        messageTxt.setText(message);
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setMessage(String message) {
        this.messageTxt.setText(message);
    }
}
