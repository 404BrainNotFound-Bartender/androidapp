package com.example.thomq.bartender_app.AsyncTasks;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;
import com.example.thomq.bartender_app.Views.Recipes.ViewRecipes;

import org.json.JSONException;
import org.json.JSONObject;

public class AddRecipeTask extends AsyncTask<Void, Void, Boolean> {
    private Recipe recipe;
    @SuppressLint("StaticFieldLeak")
    private AppCompatActivity activity;

    public AddRecipeTask(AppCompatActivity activity, Recipe recipe){
        this.activity = activity;
        this.recipe = recipe;
    }


    @Override
    protected Boolean doInBackground(Void... voids) {
        JSONObject object = RestfulUtil.getInstance().postGetData(RestfulUtil.ADD_RECIPE, recipe.toJson());
        try {
            DataService.getInstance().getRecipes().add(new Recipe(object));
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        activity.startActivity(new Intent(new Intent(activity, ViewRecipes.class)));
    }
}
