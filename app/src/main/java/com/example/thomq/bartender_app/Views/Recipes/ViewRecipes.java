package com.example.thomq.bartender_app.Views.Recipes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.example.thomq.bartender_app.AsyncTasks.LoadAllRecipesTask;
import com.example.thomq.bartender_app.R;

public class ViewRecipes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_recipes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        LinearLayout tableLayout = findViewById(R.id.allRecipeView);
        tableLayout.setGravity(Gravity.CENTER);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(ViewRecipes.this, AddRecipe.class);
            startActivity(intent);

        });
        new LoadAllRecipesTask(this, tableLayout).execute();
    }

}
