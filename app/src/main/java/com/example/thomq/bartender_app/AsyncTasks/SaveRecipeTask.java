package com.example.thomq.bartender_app.AsyncTasks;

import android.os.AsyncTask;

import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.Services.RestfulUtil;

public class SaveRecipeTask extends AsyncTask<Void, Void, Void> {

    public SaveRecipeTask(Recipe recipe) {
        this.recipe = recipe;
    }

    private Recipe recipe;


    @Override
    protected Void doInBackground(Void... voids) {
        RestfulUtil.getInstance().postData(RestfulUtil.SAVE_RECIPE, recipe.toJson());
        return null;
    }
}
