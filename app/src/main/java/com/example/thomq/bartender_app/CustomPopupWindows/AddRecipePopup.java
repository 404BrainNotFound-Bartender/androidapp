package com.example.thomq.bartender_app.CustomPopupWindows;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Button;

import com.example.thomq.bartender_app.CustomWidgets.Ingredients.IngredientInfo;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.R;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;
import com.example.thomq.bartender_app.Services.ThreadManager;

import org.json.JSONObject;

public class AddRecipePopup extends CustomPopWindowBase {
    private Ingredient ingredient;
    private IngredientInfo ingredientInfo;

    public AddRecipePopup(Context context) {
        super(context, R.layout.popup_add_ingredient);
        this.ingredientInfo  = getContentView().findViewById(R.id.popupAddIngredientInfo);
        Button createBtn = getContentView().findViewById(R.id.popupAddCreateTBtn);
        Button cancelBtn = getContentView().findViewById(R.id.popupAddCancelTBtn);

        createBtn.setOnClickListener(v -> {
            Ingredient temp = ingredientInfo.getIngredient();
            if(temp != null){

                @SuppressLint("StaticFieldLeak")
                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        ThreadManager.getInstances().execute(()->{
                            JSONObject obj = RestfulUtil.getInstance()
                                    .postGetData(RestfulUtil.ADD_INGREDIENT, temp.toJson());
                            if(obj != null) {
                                ingredient = new Ingredient(obj);
                                DataService.getInstance().getIngredients().add(ingredient);
                            }
                        });
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void o) {
                        super.onPostExecute(o);
                        AddRecipePopup.this.dismiss();
                    }
                };
                task.execute();
            }
        });

        cancelBtn.setOnClickListener(v -> dismiss());
    }

    public Ingredient getIngredient() {
        return ingredient;
    }
}
