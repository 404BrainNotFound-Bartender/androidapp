package com.example.thomq.bartender_app.Services;

import com.example.thomq.bartender_app.Models.MakeRequest;
import com.example.thomq.bartender_app.Models.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class DataService {
    private static DataService instance;

    public static DataService getInstance(){
        if (instance == null) instance = new DataService();
        return instance;
    }

    private List<Recipe> recipes;

    private List<Ingredient> ingredients;

    private List<Pump> pumps;

    private List<MakeRequest> makeRequests;

    private BartenderStatus bartenderStatus;

    private String manualModeId;

    private static final Pump noPump = new Pump();

    static {
        noPump.setName("Not Attached");
    }

    public List<Ingredient> getIngredients() {
        if(ingredients == null){
            JSONObject jsonObject = RestfulUtil.getInstance().getData(RestfulUtil.GET_INGREDIENTS);
            if(jsonObject != null) {
                ingredients = new ArrayList<>();
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    try {
                        ingredients.add(new Ingredient(jsonObject.getJSONObject(key)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                for (Ingredient ingredient:ingredients) ingredient.loadAlternatives();
            }
        }
        return ingredients;
    }


    public List<Recipe> getRecipes() {
        if(recipes == null){
            JSONObject jsonObject = RestfulUtil.getInstance().getData(RestfulUtil.GET_RECIPES);
            if(jsonObject != null) {
                recipes = new ArrayList<>();
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    try {
                        recipes.add(new Recipe(jsonObject.getJSONObject(key)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return recipes;
    }

    public List<Pump> getPumps() {
        if(pumps == null){
            JSONObject jsonObject = RestfulUtil.getInstance().getData(RestfulUtil.GET_PUMPS);
            if(jsonObject != null){
                pumps = new ArrayList<>();
                pumps.add(noPump);
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()){
                    String key = keys.next();
                    try {
                        pumps.add(new Pump(jsonObject.getJSONObject(key)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return pumps;
    }

    public List<MakeRequest> getMakeRequests() {
        if(makeRequests == null) makeRequests = new ArrayList<>();
        return makeRequests;
    }

    public BartenderStatus getBartenderStatus() {
        JSONObject object = RestfulUtil.getInstance().getData(RestfulUtil.GET_BARTENDER_STATUS);
        if(object != null) bartenderStatus = new BartenderStatus(object);
        return bartenderStatus;
    }

    public List<Ingredient> getAttachedIngredient() {
        return getIngredients().parallelStream()
                .filter(Ingredient::isAttached).collect(Collectors.toList());
    }

    public String getManualModeId() {
        return manualModeId;
    }

    public void setManualModeId(String manualModeId) {
        this.manualModeId = manualModeId;
    }

    public static Pump getNoPump() {
        return noPump;
    }
}
