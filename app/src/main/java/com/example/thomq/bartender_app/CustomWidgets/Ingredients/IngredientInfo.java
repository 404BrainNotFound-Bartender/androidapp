package com.example.thomq.bartender_app.CustomWidgets.Ingredients;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.thomq.bartender_app.ArrayAdapters.PumpArrayAdapter;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.Models.Pump;
import com.example.thomq.bartender_app.R;
import com.example.thomq.bartender_app.Services.DataService;

import java.util.Objects;

public class IngredientInfo extends ScrollView {

    private static double DENSITY_OF_WATER = 29.4848;
    private static double DENSITY_ALCOHOL = 23.3335;

    private Ingredient ingredient;

    private Context context;
    private EditText ingredientNameTxt;
    private EditText ingredientMakerTxt;
    private EditText ingredientProofTxt;
    private EditText ingredientDensityTxt;
    private Spinner  ingredientDensityUnitSpinner;
    private EditText ingredientAmountTxt;
    private Spinner  ingredientAmountUnitSpinner;
    private Spinner  ingredientPumpSpinner;


    IngredientInfo(Context context){
        super(context);
        startup(context);
    }

    public IngredientInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        startup(context);
    }

    public IngredientInfo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        startup(context);
    }

    public IngredientInfo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        startup(context);
    }

    public IngredientInfo(Context context, Ingredient ingredient){
        this(context);
        startup(ingredient);
    }


    public IngredientInfo(Context context, AttributeSet attrs, int defStyleAttr, Ingredient ingredient) {
        this(context, attrs, defStyleAttr);
        startup(ingredient);
    }

    public IngredientInfo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, Ingredient ingredient) {
        this(context, attrs, defStyleAttr, defStyleRes);
        startup(ingredient);
    }

    public boolean saveChanges(){
        if(ingredient == null) ingredient = new Ingredient();
        if(ingredientNameTxt.getText() == null || ingredientNameTxt.getText().toString().equals("")){
            Toast.makeText(context, "You need to set a name of the ingredient.", Toast.LENGTH_LONG).show();
            return false;
        }else if(ingredientMakerTxt.getText() == null || ingredientMakerTxt.getText().toString().equals("")){
            Toast.makeText(context, "You need to set the name of the maker of the ingredient.", Toast.LENGTH_LONG).show();
            return false;
        }else if(ingredientProofTxt.getText() == null || ingredientProofTxt.getText().toString().equals("")){
            Toast.makeText(context, "You need to set the proof of the ingredient.", Toast.LENGTH_LONG).show();
            return false;
        }else if(ingredientDensityTxt.getText() == null || ingredientDensityTxt.getText().toString().equals("")){
            Toast.makeText(context, "You need to set the density of the ingredient.", Toast.LENGTH_LONG).show();
            return false;
        }else if(ingredientAmountTxt.getText() == null || ingredientAmountTxt.getText().toString().equals("")){
            Toast.makeText(context, "You need to set the amount in the container.", Toast.LENGTH_LONG).show();
            return false;
        }else {
            ingredient.setName(ingredientNameTxt.getText().toString());
            ingredient.setMaker(ingredientMakerTxt.getText().toString());
            ingredient.setProof(Double.parseDouble(ingredientProofTxt.getText().toString()));
            double density = 0.0;
            double rawDensity = Double.parseDouble(ingredientDensityTxt.getText().toString());
            switch (ingredientDensityUnitSpinner.getSelectedItemPosition()){
                case 0:
                    density = rawDensity;
                    break;
                case 1:
                    density = rawDensity * 0.0295735;
                    break;
                case 2:
                    density = rawDensity * 29.5735;
                    break;
                case 3:
                    density = rawDensity * 29.5735;
                    break;
                case 4:
                    density = rawDensity * 29.5735;
                    break;
            }
            ingredient.setDensity(density);

            double rawAmount = Double.parseDouble(ingredientAmountTxt.getText().toString());
            double standardAmount = 0;
            switch (ingredientAmountUnitSpinner.getSelectedItemPosition()){
                case 0:
                    standardAmount = (rawAmount * 33.814022558919) * density;
                    break;
                case 1:
                    standardAmount = (rawAmount * 0.033814022558919) * density;
                    break;
                case 2:
                    standardAmount = rawAmount * density;
                    break;
            }

            ingredient.setRemaining(standardAmount);

            ingredient.setPump((Pump) ingredientPumpSpinner.getSelectedItem());
            ingredient.setAttached(ingredient.getPump() != DataService.getNoPump());

            return true;
        }
    }


    public Ingredient getIngredient() {
        return saveChanges() ? ingredient:null;
    }


    private void startup(Context context){
        this.context = context;
        @SuppressLint("InflateParams")
        LinearLayout layout = (LinearLayout) ((LayoutInflater) Objects.requireNonNull(context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)))
                .inflate(R.layout.ingredient_infomation_layout, null);

        ingredientNameTxt = layout.findViewById(R.id.ingredientInfoNameTxt);
        ingredientMakerTxt = layout.findViewById(R.id.ingredientInfoMakerTxt);
        ingredientProofTxt = layout.findViewById(R.id.ingredientInfoProofTxt);
        ingredientDensityTxt = layout.findViewById(R.id.ingredientInfoDensityTxt);
        ingredientDensityUnitSpinner = layout.findViewById(R.id.ingredientInfoDensityUnitSpinner);
        ingredientAmountTxt = layout.findViewById(R.id.ingredientInfoAmountTxt);
        ingredientAmountUnitSpinner = layout.findViewById(R.id.ingredientInfoAmountUnitSpinner);
        ingredientPumpSpinner = layout.findViewById(R.id.ingredientInfoPumpSpinner);


        ingredientPumpSpinner.setAdapter(new PumpArrayAdapter(context,
                android.R.layout.simple_expandable_list_item_1,
                DataService.getInstance().getPumps()));

        ingredientProofTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if(s != null && !s.toString().isEmpty()) {
                    double percent = (Double.parseDouble(s.toString()) / 2.0) / 100.0;
                    double dMix = percent * DENSITY_ALCOHOL + (1.0 - percent) * DENSITY_OF_WATER;
                    ingredientDensityTxt.setText(Double.toString(dMix));
                }
            }
        });

        addView(layout);
    }

    @SuppressLint("SetTextI18n")
    private void startup(Ingredient ingredient){
        this.ingredient = ingredient;
        ingredientNameTxt.setText(ingredient.getName());
        ingredientMakerTxt.setText(ingredient.getMaker());
        ingredientProofTxt.setText(Double.toString(ingredient.getProof()));
        ingredientDensityTxt.setText(Double.toString(ingredient.getDensity()));
        ingredientAmountTxt.setText(Double.toString(ingredient.getRemaining() / ingredient.getDensity()));
        ingredientPumpSpinner.setSelection(DataService.getInstance().getPumps().indexOf(ingredient.getPump()));
    }
}
