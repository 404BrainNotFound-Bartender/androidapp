package com.example.thomq.bartender_app.AsyncTasks;

import android.os.AsyncTask;
import android.widget.LinearLayout;

import com.example.thomq.bartender_app.CustomPopupWindows.EditIngredientPopup;
import com.example.thomq.bartender_app.CustomWidgets.Ingredients.IngredientCard;
import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Services.DataService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class LoadViewIngredientTask extends AsyncTask<AsyncModel, Double, Void> {
    private AsyncModel model;
    private LinearLayout mainView;
    private boolean fistUpdate = true;
    private List<IngredientCard> cards = new ArrayList<>();

    @Override
    protected Void doInBackground(AsyncModel... asyncModels) {
        model = asyncModels[0];
        mainView = (LinearLayout) model.getItem();
        final double numberOfIngredients = DataService.getInstance().getIngredients().size();
        AtomicInteger ingredientCounter = new AtomicInteger(0);
        DataService.getInstance().getIngredients().forEach(ingredient -> {
            IngredientCard card = new IngredientCard(model.getContext(), ingredient);
            card.setOnLongClickListener(v -> {
                EditIngredientPopup popup = new EditIngredientPopup(model.getContext(), ingredient);
                popup.show(mainView);
                return true;
            });
            publishProgress(ingredientCounter.incrementAndGet() / numberOfIngredients);
            cards.add(card);
        });
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        cards.forEach(mainView::addView);
    }
}
