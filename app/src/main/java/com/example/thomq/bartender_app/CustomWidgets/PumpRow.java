package com.example.thomq.bartender_app.CustomWidgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import com.example.thomq.bartender_app.Models.Pump;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;

public class PumpRow extends TableRow {
    private Pump pump;
    Spinner pinSpinner;
    EditText editText;

    public PumpRow(Context context) {
        super(context);
        setGravity(Gravity.CENTER_VERTICAL);
        editText = new EditText(context);
        editText.setOnEditorActionListener((v, actionId, event) -> {
            if(actionId == EditorInfo.IME_ACTION_DONE || event != null
                    && event.getAction() == KeyEvent.ACTION_DOWN){
                if(event == null || !event.isShiftPressed()){
                    if(pump == null) pump = new Pump();
                    pump.setName(editText.getText().toString());
                    return true;
                }
            }
            return false;
        });
        addView(editText);

        ArrayAdapter<Integer> pinArray = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_dropdown_item);
        for(int i = 0; i < 24; i++)pinArray.add(i);
        pinSpinner = new Spinner(context);
        pinSpinner.setAdapter(pinArray);
        pinSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(pump == null) pump = new Pump();
                if(DataService.getInstance().getPumps().parallelStream()
                        .filter(pump1 -> pump1 != DataService.getNoPump())
                        .filter(pump1 -> !pump1.getId().equals(pump.getId()))
                        .noneMatch(pump1 -> pump1.getPin() == position)){
                    if(pump == null) pump = new Pump();
                    pump.setPin(position);
                }else {
                    Toast.makeText(context,
                            "Pin " + Integer.toString(position) + " is already in uses!!",
                            Toast.LENGTH_LONG).show();
                    pinSpinner.setSelection((position + 1) % 24);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        addView(pinSpinner);
        FloatingActionButton saveBtn = new FloatingActionButton(context);
        saveBtn.setImageResource(android.R.drawable.ic_menu_save);
        saveBtn.setSize(75);
        saveBtn.setCustomSize(75);
        saveBtn.setOnClickListener(v -> {
            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    if(pump.getId() == ""){
                        Pump newPump = new Pump(RestfulUtil
                                .getInstance().postGetData(RestfulUtil.ADD_PUMP, pump.toJson()));
                        DataService.getInstance().getPumps().add(newPump);
                    }else {
                        RestfulUtil.getInstance()
                                .postData(RestfulUtil.SAVE_PUMP + "/" + pump.getId(),
                                        pump.toJson());
                    }
                    return null;
                }
            };
            String message = "";
            if(editText.getText() == null || editText.getText().toString().equals(""))
                message += "Pump needs a name";
            else pump.setName(editText.getText().toString());
            if(pump.getPin() == null) message += " and needs a pin";
//            else if(DataService.getInstance().getPumps().parallelStream()
//                    .filter(pump1 -> !pump1.getId().equals(pump.getId()))
//                    .noneMatch(pump1 -> pump1.getPin() == pump.getPin()))
//                message += " and the current pin is already in uses";
            else pump.setPin((Integer) pinSpinner.getSelectedItem());

            if(message.equals("")){
                task.execute();
            }else Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        });
        addView(saveBtn);
    }

    public PumpRow(Context context, Pump pump){
        this(context);
        this.pump = pump;
        editText.setText(this.pump.getName());
        pinSpinner.setSelection(this.pump.getPin());
    }


    public Pump getPump() {
        return pump;
    }
}
