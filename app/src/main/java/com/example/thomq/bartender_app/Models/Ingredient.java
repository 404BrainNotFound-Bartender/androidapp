package com.example.thomq.bartender_app.Models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.thomq.bartender_app.Services.DataService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Ingredient implements ModelBase{
    private String id;
    private String name;
    private String maker;
    private double density;
    private double proof;
    private double remaining;
    private Pump pump;
    private boolean attached;
    private Bitmap image;
    private List<Ingredient> alternatives = new ArrayList<>();
    private JSONArray rawAlternatives;

    public Ingredient(){}

    public Ingredient(JSONObject jsonObject){
        try {
            id = jsonObject.getString("id");
            name = jsonObject.getString("name");
            maker = jsonObject.getString("maker");
            density = jsonObject.getDouble("density");
            attached = jsonObject.getBoolean("attached");
            remaining = jsonObject.getDouble("remaining");
            proof = jsonObject.getDouble("proof");
            rawAlternatives = jsonObject.getJSONArray("alternatives");
            String pumpId = null;
            try{
                pumpId = jsonObject.getString("pump");
            }catch (JSONException e){
                pump = DataService.getNoPump();
            }finally {
                if(pumpId != null) {
                    for (Pump pump1 : DataService.getInstance().getPumps()) {
                        if (pump1.getId().equals(pumpId)) {
                            pump = pump1;
                            break;
                        }
                    }
                }
            }

//            setImage(RestfulUtil.getInstance().getData(RestfulUtil.GET_IMAGE + "0/" + id + "/").getJSONArray("image"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setImage(JSONArray array){
        byte[] tmp = new byte[array.length()];
        for(int i=0; i<array.length(); ++i) {
            try {
                tmp[i] = (byte) array.get(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        image = BitmapFactory.decodeByteArray(tmp, 0, tmp.length);
    }

    private byte[] getByteArray(){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    public double getProof() {
        return proof;
    }

    public void setProof(double proof) {
        this.proof = proof;
    }

    public double getRemaining() {
        return remaining;
    }

    public void setRemaining(double remaining) {
        this.remaining = remaining;
    }

    public boolean isAttached() {
        return attached;
    }

    public void setAttached(boolean attached) {
        this.attached = attached;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Pump getPump() {
        return pump;
    }

    public void setPump(Pump pump) {
        this.pump = pump;
    }

    public void setAlternatives(List<Ingredient> alternatives) {
        this.alternatives = alternatives;
    }

    public JSONObject toJson(){
        try {
            JSONObject payload = new JSONObject();
            payload.put("name", name);
            payload.put("maker", maker);
            payload.put("density", density);
            payload.put("proof", proof);
            payload.put("remaining", remaining);
            payload.put("attached", attached);
            JSONArray altArray = new JSONArray();
            for(Ingredient ingredient:alternatives) altArray.put(ingredient.getId());
            payload.put("alternatives", altArray);
            payload.put("pump", pump.getId());
//            byte[] tmp = getByteArray();
//            JsonArray array = new JsonArray();
//            for (byte aTmp : tmp) array.add(aTmp);
//            payload.put("image", array);
            return payload;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getDisplayName(){
        return name + "-" + maker + "-" + proof;
    }

    public void loadAlternatives(){
        if(rawAlternatives != null) {
            for (int index = 0; index < rawAlternatives.length(); index++) {
                for (Ingredient ingredient : DataService.getInstance().getIngredients()) {
                    try {
                        if (ingredient.getId().equals(rawAlternatives.getString(index))) {
                            alternatives.add(ingredient);
                            break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public List<Ingredient> getAlternatives() {
        return alternatives;
    }
}
