package com.example.thomq.bartender_app.ArrayAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.thomq.bartender_app.Models.Ingredient;

import java.util.List;

public class IngredientListViewAdapter extends BaseAdapter {
    private Context context;
    private List<Ingredient> ingredients;
    private LayoutInflater inflater;

    public IngredientListViewAdapter(@NonNull Context context,
                                  @NonNull List<Ingredient> objects) {
        this.context = context;
        this.ingredients = objects;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View holder;
        if(convertView == null){
            holder = new View(context);
            convertView = inflater.inflate(android.R.layout.activity_list_item, null);

        }
        return null;
    }
}
