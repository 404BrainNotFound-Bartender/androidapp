package com.example.thomq.bartender_app.CustomWidgets.Recipes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.thomq.bartender_app.AsyncTasks.SetRecipeRatingTask;
import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.R;

@SuppressLint("ViewConstructor")
public class RecipeCard extends CardView {
    private Recipe recipe;

    public RecipeCard(@NonNull Context context, Recipe recipe) {
        super(context);
        LayoutParams params = new LayoutParams(
                Toolbar.LayoutParams.WRAP_CONTENT,
                Toolbar.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER
        );
        this.recipe = recipe;
        setBackgroundResource(R.drawable.recipe_default);
        params.setMargins(10,10,10,10);
        setLayoutParams(params);
        setCardBackgroundColor(Color.WHITE);
        setMaxCardElevation(15);
        setRadius(9);
        setCardElevation(9);
        setContentPadding(15, 15, 15, 15);

        LinearLayout vLayout = new LinearLayout(context);
        vLayout.setOrientation(LinearLayout.VERTICAL);
        vLayout.setLayoutParams(params);

        TextView nameTxt = new TextView(context);
        nameTxt.setTextSize(18);
        nameTxt.setTextColor(Color.BLACK);
        nameTxt.setLayoutParams(params);
        nameTxt.setText(recipe.getName());
        vLayout.addView(nameTxt);

        RatingBar ratingBar = new RatingBar(context);
        ratingBar.setNumStars(5);
        ratingBar.setStepSize((float) 0.25);
        ratingBar.setRating((float) recipe.getRating());
        ratingBar.setLayoutParams(params);
        ratingBar.setIsIndicator(true);
        vLayout.addView(ratingBar);
//        TextView serving = new TextView(context);
//        serving.setTextSize(18);
//        serving.setTextColor(Color.BLACK);
//        serving.setLayoutParams(params);
//        serving.setText("Serving size: " + Double.toString(recipe.getServingSize()) + "oz");
//        vLayout.addView(serving);

        addView(vLayout);
        setLayoutParams(params);
    }

    public Recipe getRecipe() {
        return recipe;
    }
}
