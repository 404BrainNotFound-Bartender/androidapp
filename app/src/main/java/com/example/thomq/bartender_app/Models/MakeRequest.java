package com.example.thomq.bartender_app.Models;

import com.example.thomq.bartender_app.Services.DataService;

import org.json.JSONException;
import org.json.JSONObject;

public class MakeRequest implements ModelBase {
    private String id;
    private Recipe recipe;
    private Integer amount;
    private int position;

    public MakeRequest(){}

    public MakeRequest(JSONObject object){
        try {
            id = object.getString("id");
            for(Recipe recipe1:DataService.getInstance().getRecipes()){
                if(recipe1.getId().equals(object.getString("recipe"))){
                    recipe = recipe1;
                    break;
                }
            }
            amount = object.getInt("amount");
            position = object.getInt("position");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public JSONObject toJson() {
        return null;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
