package com.example.thomq.bartender_app.CustomWidgets.Ingredients;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;

import com.example.thomq.bartender_app.CustomPopupWindows.PopupSelectIngredient;
import com.example.thomq.bartender_app.Models.Ingredient;

public class IngredientButton extends  AppCompatButton{
    private Ingredient ingredient;
    private Context context;

    public IngredientButton(Context context) {
        super(context);
        setup(context);
    }

    public IngredientButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context);
    }

    public IngredientButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup(context);
    }

    public IngredientButton(Context context, Ingredient ingredient) {
        super(context);
        setup(context);
        this.ingredient = ingredient;
        setText(ingredient.getDisplayName());
    }

    @SuppressLint("SetTextI18n")
    private void setup(Context context){
        this.context = context;
        setText("Select Ingredient");
        setOnClickListener(this::selectIngredient);
    }

    private void selectIngredient(View view){
        PopupSelectIngredient popup = new PopupSelectIngredient(context);
        popup.show(view);
    }

    public Ingredient getIngredient() {
        return ingredient;
    }
}
