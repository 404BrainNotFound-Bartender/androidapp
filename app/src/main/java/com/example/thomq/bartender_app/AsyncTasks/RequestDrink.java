package com.example.thomq.bartender_app.AsyncTasks;

import android.os.AsyncTask;

import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.MakeRequest;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;
import com.example.thomq.bartender_app.Services.ThreadManager;

public class RequestDrink extends AsyncTask<AsyncModel, Void, MakeRequest> {
    private AsyncModel asyncModel;
    @Override
    protected MakeRequest doInBackground(AsyncModel... models) {
        asyncModel = models[0];
        MakeRequest request = (MakeRequest) asyncModel.getModel();
        MakeRequest makeRequest = new MakeRequest(
                RestfulUtil.getInstance()
                        .getData(RestfulUtil.MAKE_RECIPE
                                + request.getRecipe().getId() + "/"
                                + request.getAmount().toString() + "/"));
        DataService.getInstance()
                .getMakeRequests().add(makeRequest);
        return makeRequest;
    }

    @Override
    protected void onPostExecute(MakeRequest request) {
        MakeDrink makeDrink = new MakeDrink();
        AsyncModel asyncModel1 = new AsyncModel(
                asyncModel.getContext(), request);
        makeDrink.executeOnExecutor(
                ThreadManager.getInstances().getService(),
                asyncModel1);
    }
}
