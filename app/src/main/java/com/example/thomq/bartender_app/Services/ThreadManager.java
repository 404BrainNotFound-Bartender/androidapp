package com.example.thomq.bartender_app.Services;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadManager {
    private static ThreadManager instances;
    private ExecutorService service = Executors
            .newScheduledThreadPool(Runtime.getRuntime().availableProcessors());

    public static ThreadManager getInstances() {
        if(instances == null)instances = new ThreadManager();
        return instances;
    }

    public void execute(Runnable runnable){
        service.execute(runnable);
    }

    public ExecutorService getService() {
        return service;
    }

    public Future submit(Runnable runnable){
        return service.submit(runnable);
    }
}
