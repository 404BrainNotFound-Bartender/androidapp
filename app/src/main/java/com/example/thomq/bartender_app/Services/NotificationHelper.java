package com.example.thomq.bartender_app.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;

import com.example.thomq.bartender_app.R;

public class NotificationHelper extends ContextWrapper {
    private NotificationManager notifManager;
    public static final String DRINK_STATUS_ID = "drinkStatusId";
    public static final String DRINK_STATUS_CHANNEL = "Drink Status Channel";
    public static final String CHANNEL_TWO_ID = "com.jessicathornsby.myapplication.TWO";
    public static final String CHANNEL_TWO_NAME = "Channel Two";

    public NotificationHelper(Context base) {
        super(base);
        createChannels();
    }

    private void createChannels() {
        NotificationChannel drinkStatusChannel = new NotificationChannel(DRINK_STATUS_ID,
                DRINK_STATUS_CHANNEL, NotificationManager.IMPORTANCE_DEFAULT);
        drinkStatusChannel.enableLights(true);
        drinkStatusChannel.setLightColor(Color.BLUE);
        drinkStatusChannel.setShowBadge(true);
        drinkStatusChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(drinkStatusChannel);

        NotificationChannel notificationChannel2 = new NotificationChannel(CHANNEL_TWO_ID,
                CHANNEL_TWO_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel2.enableLights(false);
        notificationChannel2.enableVibration(true);
        notificationChannel2.setLightColor(Color.GREEN);
        notificationChannel2.setShowBadge(false);
        getManager().createNotificationChannel(notificationChannel2);
    }

    public Notification.Builder getDrinkStatusNotification(String title, String body) {
        return new Notification.Builder(getApplicationContext(), DRINK_STATUS_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.recipe_default)
                .setAutoCancel(true);
    }

    public Notification.Builder getNotification2(String title, String body) {
        return new Notification.Builder(getApplicationContext(), CHANNEL_TWO_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(android.R.drawable.alert_dark_frame)
                .setAutoCancel(true);
    }


    public void notify(int id, Notification.Builder notification) {
        getManager().notify(id, notification.build());
    }

    private NotificationManager getManager() {
        if(notifManager == null)
            notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return notifManager;
    }
}
