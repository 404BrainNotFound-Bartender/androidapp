package com.example.thomq.bartender_app.CustomWidgets.Ingredients;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.R;

public class IngredientStatusRow extends TableRow {
    public IngredientStatusRow(Context context, Ingredient ingredient) {
        super(context);
        ImageView imageView = new ImageView(context);
        if(ingredient.isAttached())imageView.setImageResource(R.drawable.check_mark);
        else imageView.setImageResource(R.drawable.x);
        addView(imageView);

        TextView nameView = new TextView(context);
        nameView.setText(ingredient.getDisplayName());
        addView(nameView);
    }
}
