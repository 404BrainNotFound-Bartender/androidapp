package com.example.thomq.bartender_app.Models;


public class DispenseModel {
    private Ingredient ingredient;
    private Integer amount;

    public DispenseModel(Ingredient ingredient, Double amount){
        this.ingredient = ingredient;
        amount *= 100;
        this.amount = amount.intValue();
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public Integer getAmount() {
        return amount;
    }
}
