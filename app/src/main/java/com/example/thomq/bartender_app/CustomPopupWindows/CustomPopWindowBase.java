package com.example.thomq.bartender_app.CustomPopupWindows;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import java.util.Objects;


public abstract class CustomPopWindowBase extends PopupWindow {

    public CustomPopWindowBase(Context context){
        super(context);
        setOutsideTouchable(true);
        setFocusable(true);
    }

    public CustomPopWindowBase(Context context, int layoutId) {
        this(context);
        setContentView(((LayoutInflater) Objects.requireNonNull(context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)))
                .inflate(layoutId, null));
    }

    public void show(View parent){
        showAtLocation(parent, Gravity.CENTER, 0, 0);
    }
}
