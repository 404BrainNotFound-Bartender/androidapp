package com.example.thomq.bartender_app.AsyncTasks;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.text.InputFilter;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thomq.bartender_app.CustomWidgets.Recipes.RecipeCard;
import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.MakeRequest;
import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.DecimalDigitsInputFilter;

import java.util.List;
import java.util.stream.Collectors;

public class GetRecipes extends AsyncTask<AsyncModel, Void, List<Recipe>> {
    private AsyncModel asyncModel;
    @SuppressLint("StaticFieldLeak")
    private LinearLayout recipeArea;

    @Override
    protected List<Recipe> doInBackground(AsyncModel... asyncModels) {
        asyncModel = asyncModels[0];
        recipeArea = (LinearLayout) asyncModel.getItem();
        try {
            return DataService.getInstance().getRecipes();
        }catch (NullPointerException e){
            e.printStackTrace();
            return null;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onPostExecute(List<Recipe> recipes) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) asyncModel.getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        asyncModel.getDialog().dismiss();
        if(recipes != null) {
            List<Recipe> canMake = DataService.getInstance().getRecipes().parallelStream()
                    .filter(Recipe::isCanMake).collect(Collectors.toList());
            if(!canMake.isEmpty()){
                canMake.forEach(recipe -> {
                    RecipeCard card = new RecipeCard(asyncModel.getContext(), recipe);
                    card.setOnClickListener(v -> {
                        PopupWindow popupWindow = new PopupWindow(asyncModel.getContext());
                        LinearLayout mainLayout = new LinearLayout(asyncModel.getContext());
                        mainLayout.setOrientation(LinearLayout.VERTICAL);
                        TextView message = new TextView(asyncModel.getContext());
                        message.setText("The Amount You Would Like To Make:");
                        mainLayout.addView(message);

                        EditText amountToMake = new EditText(asyncModel.getContext());
                        amountToMake.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        amountToMake.setFilters(new InputFilter[]{
                                new DecimalDigitsInputFilter(5, 2)});
                        amountToMake.setMinimumWidth(50);
                        TextView textView = new TextView(asyncModel.getContext());
                        textView.setText(" oz");
                        LinearLayout linearLayout = new LinearLayout(asyncModel.getContext());
                        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                        linearLayout.addView(amountToMake);
                        linearLayout.addView(textView);
                        mainLayout.addView(linearLayout);

                        Button makeBtn = new Button(asyncModel.getContext());
                        makeBtn.setText("Make Drink");
                        makeBtn.setOnClickListener(v1 -> {
                            MakeRequest request = new MakeRequest();
                            request.setAmount(
                                    ((Double)(Double.parseDouble(
                                            amountToMake.getText().toString())*100.0)).intValue());
                            request.setRecipe(recipe);
                            AsyncModel model = new AsyncModel(asyncModel.getContext(), request);
                            RequestDrink drink = new RequestDrink();
                            drink.execute(model);
                            popupWindow.dismiss();
                        });
                        mainLayout.addView(makeBtn);

                        mainLayout.setBackgroundColor(Color.WHITE);
                        mainLayout.measure(View.MeasureSpec.makeMeasureSpec(popupWindow.getWidth(),
                                View.MeasureSpec.EXACTLY),
                                View.MeasureSpec.makeMeasureSpec(0,
                                        View.MeasureSpec.UNSPECIFIED));

                        popupWindow.setContentView(mainLayout);
                        popupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
                        popupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
                        popupWindow.setFocusable(true);
                        popupWindow.showAtLocation(recipeArea, Gravity.CENTER,0,0);
                    });
                    recipeArea.addView(card);
                });
            }
        }else {
            Toast.makeText(asyncModel.getContext(), "No Recipes Found",
                    Toast.LENGTH_LONG).show();
        }
    }
}
