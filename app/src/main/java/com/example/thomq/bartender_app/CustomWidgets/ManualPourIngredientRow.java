package com.example.thomq.bartender_app.CustomWidgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.thomq.bartender_app.AsyncTasks.DispenseIngredientTask;
import com.example.thomq.bartender_app.ArrayAdapters.IngredientArrayAdapter;
import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.DispenseModel;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.Services.DataService;

public class ManualPourIngredientRow extends TableRow {
    private Spinner ingredientSpinner;
    private Ingredient ingredient;
    private Double amount = 0.0;

    private final static double SMALL_ADD_AMOUNT = 0.05;
    private final static double BIG_ADD_AMOUNT = 1.0;


    @SuppressLint("ClickableViewAccessibility")
    public ManualPourIngredientRow(Context context) {
        super(context);
        setLayoutParams(new TableLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        ));

        double screenDensity = context.getResources().getDisplayMetrics().density;
        int spinnerWidth = (int) (171.4285714285714 * screenDensity);
        int decimalWidth = (int) (57.14285714285714 * screenDensity);
        int intAddWidth = (int) (76.19047619047619 * screenDensity);


        IngredientArrayAdapter adapter = new IngredientArrayAdapter(context,
                android.R.layout.simple_dropdown_item_1line,
                DataService.getInstance().getAttachedIngredient());
        ingredientSpinner = new Spinner(context);
        ingredientSpinner.setAdapter(adapter);
        ingredientSpinner.setLayoutParams(new TableRow.LayoutParams(
                spinnerWidth,
                LayoutParams.WRAP_CONTENT
        ));

        System.out.println(context.getResources().getDisplayMetrics().density);

        addView(ingredientSpinner);

        TextView amountTxt = new TextView(context);
        amountTxt.setText(formatDouble());
        addView(amountTxt);

        Button decimalAddBtn = new Button(context);
        decimalAddBtn.setText("+");
        decimalAddBtn.setLayoutParams(new TableRow.LayoutParams(
                decimalWidth,
                LayoutParams.WRAP_CONTENT
        ));
        decimalAddBtn.setOnClickListener(v -> {
            if(ingredient == null) ingredient = (Ingredient) ingredientSpinner.getSelectedItem();
            new DispenseIngredientTask()
                    .execute(new AsyncModel(context, new DispenseModel(ingredient, SMALL_ADD_AMOUNT)));
            amount += SMALL_ADD_AMOUNT;
            amountTxt.setText(formatDouble());
        });
        addView(decimalAddBtn);

        Button intAddBtn = new Button(context);
        intAddBtn.setText("++");
        intAddBtn.setLayoutParams(new TableRow.LayoutParams(
                intAddWidth,
                LayoutParams.WRAP_CONTENT
        ));
        intAddBtn.setOnClickListener(v -> {
            if(ingredient == null) ingredient = (Ingredient) ingredientSpinner.getSelectedItem();
            new DispenseIngredientTask()
                    .execute(new AsyncModel(context, new DispenseModel(ingredient, BIG_ADD_AMOUNT)));
            amount += BIG_ADD_AMOUNT;
            amountTxt.setText(formatDouble());
        });
        addView(intAddBtn);

    }

    public Double getAmount() {
        return amount;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }


    @SuppressLint("DefaultLocale")
    private String formatDouble(){
        return String.format("%.2f  oz", amount);
    }
}
