package com.example.thomq.bartender_app.Services;

import android.content.Context;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyHander {

    private static PropertyHander instance;

    private final static String BARTENDER_IP_KEY = "bartender.ip";
    private final static String PROPERTIES_FILE_NAME = "app.properties";

    private Properties properties;

    public static PropertyHander getInstance() {
        if(instance == null)instance = new PropertyHander();
        return instance;
    }

    public void loadProperties(Context context){

        try {
            InputStream rawResource = context.getAssets().open(PROPERTIES_FILE_NAME);
            properties = new Properties();
            properties.load(rawResource);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getBartenderIp(){
        return properties.getProperty(BARTENDER_IP_KEY);
    }

    public boolean savePropterties(){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(PROPERTIES_FILE_NAME);
            properties.store(fileOutputStream, null);
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setBartenderIp(String ip) throws IOException {
        properties.setProperty(BARTENDER_IP_KEY, ip);
    }
}
