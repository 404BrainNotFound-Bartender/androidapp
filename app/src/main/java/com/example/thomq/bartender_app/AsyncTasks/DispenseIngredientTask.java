package com.example.thomq.bartender_app.AsyncTasks;

import android.os.AsyncTask;

import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.DispenseModel;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;

public class DispenseIngredientTask extends AsyncTask<AsyncModel, Void, Void> {
    private AsyncModel model;
    @Override
    protected Void doInBackground(AsyncModel... models) {
        this.model = models[0];
        DispenseModel dispenseModel = (DispenseModel) model.getItem();
        RestfulUtil.getInstance().getData(RestfulUtil.DISPENSE_INGREDIENT
                + DataService.getInstance().getManualModeId() + "/" +
                dispenseModel.getIngredient().getId() + "/" + dispenseModel.getAmount() + "/");
        return null;
    }
}
