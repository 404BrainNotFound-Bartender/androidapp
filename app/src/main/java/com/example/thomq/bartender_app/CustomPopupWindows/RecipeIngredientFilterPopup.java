package com.example.thomq.bartender_app.CustomPopupWindows;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;

import com.example.thomq.bartender_app.CustomWidgets.Ingredients.IngredientFilterRow;
import com.example.thomq.bartender_app.CustomWidgets.Recipes.RecipeCard;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.R;

import java.util.ArrayList;
import java.util.List;

public class RecipeIngredientFilterPopup extends CustomPopWindowBase {
    private List<Ingredient> ingredientList = new ArrayList<>();
    private TableLayout ingredientTable;
    private ViewGroup recipeArea;

    public RecipeIngredientFilterPopup(Context context, ViewGroup recipeArea){
        super(context, R.layout.ingredient_filter);
        this.recipeArea = recipeArea;
        Button applyFilterBtn = getContentView().findViewById(R.id.ingredientFilterApplyBtn);
        ingredientTable = getContentView().findViewById(R.id.ingredientFilterTable);

        ingredientTable.addView(new IngredientFilterRow(context, ingredientTable));

        applyFilterBtn.setOnClickListener(this::handleApplyFilter);

        FloatingActionButton addIngredient = getContentView().findViewById(R.id.ingredientFilterAddIngredientFab);
        addIngredient.setOnClickListener(v -> ingredientTable.addView(new IngredientFilterRow(context, ingredientTable)));

    }


    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    private boolean handleApplyFilter(View view){
        for(int i  = 0; i < ingredientTable.getChildCount(); i++)
            ingredientList.add(((IngredientFilterRow)ingredientTable.getChildAt(i)).getIngredient());

        List<RecipeCard> toRemove = new ArrayList<>();
        for(int i = 0; i < recipeArea.getChildCount(); i++){
            RecipeCard card = (RecipeCard) recipeArea.getChildAt(i);
            if(!card.getRecipe().getIngredients().keySet().containsAll(ingredientList)) {
                toRemove.add(card);
            }
        }
        toRemove.forEach(recipeArea::removeView);
        dismiss();
        return false;
    }


}
