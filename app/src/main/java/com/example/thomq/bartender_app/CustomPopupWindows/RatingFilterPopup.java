package com.example.thomq.bartender_app.CustomPopupWindows;

import android.content.Context;
import android.text.InputFilter;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.thomq.bartender_app.CustomWidgets.Recipes.RecipeCard;
import com.example.thomq.bartender_app.R;
import com.example.thomq.bartender_app.Services.DecimalDigitsInputFilter;
import com.example.thomq.bartender_app.Services.MaxValueFilter;

import java.util.ArrayList;
import java.util.List;

public class RatingFilterPopup extends CustomPopWindowBase {

    private EditText minRatingTxt;
    private Button applyBtn;
    private ViewGroup recipeArea;

    public RatingFilterPopup(Context context, ViewGroup recipeArea) {
        super(context, R.layout.recipe_rating_filter_popup);
        minRatingTxt = getContentView().findViewById(R.id.ratingMinTxt);
        applyBtn = getContentView().findViewById(R.id.ratingFilterApplyBtn);
        this.recipeArea = recipeArea;

        minRatingTxt.setFilters(new InputFilter[]{
                new DecimalDigitsInputFilter(1,2),
                new MaxValueFilter(5.0)});

        applyBtn.setOnClickListener(this::applyFilter);
    }

    private boolean applyFilter(View view){
        if(minRatingTxt.getText() != null && !minRatingTxt.getText().toString().isEmpty()) {
            Double minRating = Double.parseDouble(minRatingTxt.getText().toString());
            List<RecipeCard> toRemove = new ArrayList<>();
            for (int i = 0; i < recipeArea.getChildCount(); i++) {
                RecipeCard card = (RecipeCard) recipeArea.getChildAt(i);
                if (card.getRecipe().getRating() <= minRating) {
                    toRemove.add(card);
                }
            }
            toRemove.forEach(recipeArea::removeView);
            dismiss();
        }
        return false;
    }
}
