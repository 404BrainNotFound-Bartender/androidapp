package com.example.thomq.bartender_app.CustomWidgets.Ingredients;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.StackView;
import android.widget.TextView;

import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.R;

public class IngredientCard extends CardView {

    private Context context;
    private Ingredient ingredient;

    public IngredientCard(@NonNull Context context, Ingredient ingredient) {
        super(context);
        this.context = context;
        this.ingredient = ingredient;

        StackView stack = new StackView(context);
//        ImageView imageView = new ImageButton(context);
//        imageView.setImageBitmap(ingredient.getImage());
//        imageView.setMaxWidth(50);
//        imageView.setMinimumWidth(50);
//        stack.addView(imageView);

        setBackgroundResource(R.drawable.ingredient_default_200_light);

        LayoutParams params = new LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER
        );
        params.setMargins(10,10,10,10);
        setLayoutParams(params);
        setCardBackgroundColor(Color.WHITE);
        setMaxCardElevation(15);
        setRadius(9);
        setCardElevation(9);
        setContentPadding(15, 15, 15, 15);

        LinearLayout vLayout = new LinearLayout(context);
        vLayout.setOrientation(LinearLayout.VERTICAL);
        vLayout.setLayoutParams(params);
        vLayout.setGravity(Gravity.CENTER);

        TextView nameTxt = new TextView(context);
        nameTxt.setTextSize(18);
        nameTxt.setTextColor(Color.BLACK);
        nameTxt.setLayoutParams(params);
        nameTxt.setText(ingredient.getName());
        vLayout.addView(nameTxt);

        TextView makerTxt = new TextView(context);
        makerTxt.setTextSize(18);
        makerTxt.setTextColor(Color.BLACK);
        makerTxt.setLayoutParams(params);
        makerTxt.setText(ingredient.getMaker());
        vLayout.addView(makerTxt);

        TextView proofTxt = new TextView(context);
        proofTxt.setTextSize(18);
        proofTxt.setTextColor(Color.BLACK);
        proofTxt.setLayoutParams(params);
        proofTxt.setText(Double.toString(ingredient.getProof()));
        vLayout.addView(proofTxt);

        TextView remainingTxt = new TextView(context);
        remainingTxt.setTextSize(18);
        remainingTxt.setTextColor(Color.BLACK);
        remainingTxt.setText(Double.toString(ingredient.getRemaining() / ingredient.getDensity()) + " oz");
        vLayout.addView(remainingTxt);

//        stack.addView(vLayout);
        addView(vLayout);
    }
}
