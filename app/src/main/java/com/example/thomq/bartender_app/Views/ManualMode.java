package com.example.thomq.bartender_app.Views;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.thomq.bartender_app.AsyncTasks.LeaveManualModeTask;
import com.example.thomq.bartender_app.CustomWidgets.ComponentRow;
import com.example.thomq.bartender_app.CustomWidgets.ManualPourIngredientRow;
import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.R;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ManualMode extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_mode);
        TableLayout table = findViewById(R.id.manualModeIngredientTable);
        table.addView(new ManualPourIngredientRow(this));

        FloatingActionButton fab = findViewById(R.id.manualMaodeAddIngredient);
        fab.setOnClickListener(v -> {
            table.addView(new ManualPourIngredientRow(this));
        });

        Button finishBtn = findViewById(R.id.manualModeFinishBtn);
        Button finishSaveBtn = findViewById(R.id.manualModeFinishSaveBtn);

        finishBtn.setOnClickListener(v -> {
            new LeaveManualModeTask().execute();
            startActivity(new Intent(ManualMode.this, MainActivity.class));
        });
        finishSaveBtn.setOnClickListener(v -> {
            PopupWindow popupWindow = new PopupWindow(ManualMode.this);
            LinearLayout mainLayout = new LinearLayout(ManualMode.this);
            mainLayout.setOrientation(LinearLayout.VERTICAL);

            LinearLayout topLayer = new LinearLayout(ManualMode.this);
            topLayer.setOrientation(LinearLayout.HORIZONTAL);
            TextView view = new TextView(ManualMode.this);
            view.setText("Recipe Name: ");
            EditText text = new EditText(ManualMode.this);
            text.setMinWidth(200);
            topLayer.addView(view);
            topLayer.addView(text);
            mainLayout.addView(topLayer);

            Button saveBtn = new Button(ManualMode.this);
            saveBtn.setText("Save Recipe");
            mainLayout.addView(saveBtn);

            saveBtn.setOnClickListener(v1 -> {
                double totalAmount = 0;
                double volume = 0;
                for(int index=0; index < table.getChildCount(); index++) {
                    ManualPourIngredientRow row = (ManualPourIngredientRow) table.getChildAt(index);
                    totalAmount += row.getAmount();
                    volume += row.getAmount() * row.getIngredient().getDensity();
                }
                JSONObject payload = new JSONObject();
                try {
                    payload.put("name", text.getText().toString());
                    JSONArray array = new JSONArray();
                    for(int index=0; index < table.getChildCount(); index++){
                        JSONObject comps = new JSONObject();
                        ManualPourIngredientRow row = (ManualPourIngredientRow) table.getChildAt(index);
                        comps.put("id", row.getIngredient().getId());
                        comps.put("amount",
                                (row.getAmount() * row.getIngredient().getDensity()) / totalAmount);
                        array.put(comps);
                    }
                    payload.put("components", array);
                    payload.put("serving", totalAmount);
                    payload.put("density", totalAmount/volume);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                @SuppressLint("StaticFieldLeak")
                AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        JSONObject object = RestfulUtil.getInstance().postGetData(RestfulUtil.ADD_RECIPE, payload);
                        try {
                            DataService.getInstance().getRecipes().add(new Recipe(object));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
                asyncTask.execute();
                new LeaveManualModeTask().execute();
                startActivity(new Intent(ManualMode.this, MainActivity.class));
            });

            popupWindow.setContentView(mainLayout);
            popupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            popupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
            popupWindow.setFocusable(true);
            popupWindow.showAsDropDown(table);
        });
    }
}
