package com.example.thomq.bartender_app.Services;

import android.text.InputFilter;
import android.text.Spanned;

public class MaxValueFilter implements InputFilter {
    private double maxValue;

    public MaxValueFilter(double maxValue){
        this.maxValue = maxValue;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            double input = Double.parseDouble(dest.toString() + source.toString());
            if (input <= maxValue)
                return null;
        } catch (NumberFormatException ignored) { }
        return "";
    }
}
