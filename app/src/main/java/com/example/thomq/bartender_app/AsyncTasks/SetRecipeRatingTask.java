package com.example.thomq.bartender_app.AsyncTasks;

import android.os.AsyncTask;

import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.Services.RestfulUtil;

public class SetRecipeRatingTask extends AsyncTask<Void, Void, Void> {
    private double rating;
    private Recipe recipe;

    public SetRecipeRatingTask(Recipe recipe, double rating){
        this.recipe = recipe;
        this.rating = rating;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Long r = Math.round(rating * 100.0);
        RestfulUtil.getInstance().getData(RestfulUtil.RATE_RECIPE
                + recipe.getId() + "/" + r.toString());
        return null;
    }
}
