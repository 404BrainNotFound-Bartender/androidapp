package com.example.thomq.bartender_app.Views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.example.thomq.bartender_app.AsyncTasks.EnterManualModeTask;
import com.example.thomq.bartender_app.AsyncTasks.GetRecipes;
import com.example.thomq.bartender_app.CustomPopupWindows.CustomPopWindowBase;
import com.example.thomq.bartender_app.CustomPopupWindows.RatingFilterPopup;
import com.example.thomq.bartender_app.CustomPopupWindows.RecipeIngredientFilterPopup;
import com.example.thomq.bartender_app.CustomPopupWindows.RecipeNameFilterPopup;
import com.example.thomq.bartender_app.CustomWidgets.Recipes.RecipeCard;
import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.Recipe;
import com.example.thomq.bartender_app.R;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Views.Ingredients.AddIngredient;
import com.example.thomq.bartender_app.Views.Ingredients.ViewIngredients;
import com.example.thomq.bartender_app.Views.Pumps.PumpManage;
import com.example.thomq.bartender_app.Views.Recipes.AddRecipe;
import com.example.thomq.bartender_app.Views.Recipes.ViewRecipes;

import java.util.List;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class MainActivity extends AppCompatActivity {
    private List<Recipe> filteredRecipes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        PropertyHander.getInstance().loadProperties(this);

        FloatingActionButton enterManualBtn = findViewById(R.id.enterManualBtn);
        enterManualBtn.setOnClickListener(view ->{
            EnterManualModeTask task = new EnterManualModeTask();
            AsyncModel model = new AsyncModel(MainActivity.this);
            task.execute(model);
        });




        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Getting Recipes From Sever");
        progressDialog.show();

        LinearLayout recipeArea = findViewById(R.id.makeAbleRecipeField);
        AsyncModel model = new AsyncModel(this, recipeArea, progressDialog);
        GetRecipes getRecipes = new GetRecipes();
        getRecipes.execute(model);

        FabSpeedDial filterDial = findViewById(R.id.filterBtn);
        filterDial.setMenuListener(new SimpleMenuListenerAdapter(){
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                CustomPopWindowBase popup = null;
                switch (menuItem.getItemId()){
                    case R.id.byIngredientFilter:
                        popup = new RecipeIngredientFilterPopup(MainActivity.this, recipeArea);
                        break;
                    case R.id.clearRecipeFilter:
                        recipeArea.removeAllViews();
                        DataService.getInstance().getRecipes().stream().filter(Recipe::isCanMake)
                        .forEach(recipe -> recipeArea.addView(new RecipeCard(MainActivity.this, recipe)));
                        break;
                    case R.id.byNameFilter:
                        popup = new RecipeNameFilterPopup(MainActivity.this, recipeArea);
                        break;
                    case R.id.byRatingFilter:
                        popup = new RatingFilterPopup(MainActivity.this, recipeArea);
                        break;
                }
                if(popup != null) popup.show(recipeArea);
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.addIngredient:
                intent = new Intent(this, AddIngredient.class);
                break;
            case R.id.viewIngredients:
                intent = new Intent(this, ViewIngredients.class);
                break;
            case R.id.addRecipeBtn:
                intent = new Intent(this, AddRecipe.class);
                break;
            case R.id.viewAllRecipe:
                intent = new Intent(this, ViewRecipes.class);//new Intent(this, ViewIngredients.class);
                break;
//            case  R.id.action_settings:
//                intent = new Intent(this, MainSettingScreen.class);
//                break;
            case R.id.addPumpItem:
                intent = new Intent(this, PumpManage.class);
                break;
            default:
                intent = null;
        }
        if(intent != null)startActivity(intent);
        return super.onOptionsItemSelected(item);
    }
}