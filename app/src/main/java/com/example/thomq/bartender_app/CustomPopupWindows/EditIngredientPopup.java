package com.example.thomq.bartender_app.CustomPopupWindows;

import android.content.Context;
import android.graphics.Color;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.thomq.bartender_app.AsyncTasks.SaveIngredientTask;
import com.example.thomq.bartender_app.CustomWidgets.Ingredients.IngredientInfo;
import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.Ingredient;

public class EditIngredientPopup extends CustomPopWindowBase {
    public EditIngredientPopup(Context context, Ingredient ingredient) {
        super(context);
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setBackgroundColor(Color.WHITE);
        IngredientInfo ingredientInfo = new IngredientInfo(context, ingredient);
        layout.addView(ingredientInfo);


        LinearLayout btnLayout = new LinearLayout(context);
        btnLayout.setOrientation(LinearLayout.HORIZONTAL);

        Button saveBtn = new Button(context);
        saveBtn.setText("Save");
        saveBtn.setOnClickListener(v -> {
            Ingredient ingredient1 = ingredientInfo.getIngredient();
            if(ingredient1 != null) new SaveIngredientTask().execute(new AsyncModel(context, ingredient1));
            dismiss();
        });
        btnLayout.addView(saveBtn);

        Button retrainBtn = new Button(context);
        retrainBtn.setText("Retain");
        btnLayout.addView(retrainBtn);

        layout.addView(btnLayout);

        setContentView(layout);
    }
}
