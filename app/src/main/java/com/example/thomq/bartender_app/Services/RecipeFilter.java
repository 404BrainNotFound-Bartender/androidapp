package com.example.thomq.bartender_app.Services;

import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.Models.Recipe;

import java.util.List;
import java.util.stream.Collectors;

public class RecipeFilter {

    public static List<Recipe> filterRecipeByIngredient(List<Recipe> toFilter, List<Ingredient> ingredients){
        return toFilter.parallelStream()
                .filter(recipe -> recipe.getIngredients().keySet().containsAll(ingredients))
                .collect(Collectors.toList());
    }

    public static List<Recipe> filterRecipeByName(List<Recipe> toFilter, String name){
        return toFilter.parallelStream()
                .filter(recipe -> recipe.getName().startsWith(name))
                .collect(Collectors.toList());
    }
}
