package com.example.thomq.bartender_app.AsyncTasks;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.LinearLayout;

import com.example.thomq.bartender_app.CustomPopupWindows.EditRecipePopup;
import com.example.thomq.bartender_app.CustomWidgets.Recipes.RecipeCard;
import com.example.thomq.bartender_app.Services.DataService;

public class LoadAllRecipesTask extends AsyncTask<Void, Void, Void> {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    @SuppressLint("StaticFieldLeak")
    private LinearLayout tableLayout;


    public LoadAllRecipesTask(Context context, LinearLayout tableLayout) {
        this.context = context;
        this.tableLayout = tableLayout;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        DataService.getInstance().getRecipes();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        DataService.getInstance().getRecipes().forEach(recipe -> {
            RecipeCard card = new RecipeCard(context, recipe);
            card.setLongClickable(true);
            card.setOnLongClickListener(v -> {
                EditRecipePopup popup = new EditRecipePopup(context, recipe);
                popup.show(tableLayout);
                return true;
            });
            tableLayout.addView(card);
        });
    }
}
