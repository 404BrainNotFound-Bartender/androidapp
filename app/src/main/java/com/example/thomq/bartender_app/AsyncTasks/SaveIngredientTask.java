package com.example.thomq.bartender_app.AsyncTasks;

import android.os.AsyncTask;

import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.Services.RestfulUtil;

public class SaveIngredientTask extends AsyncTask<AsyncModel, Void, Void> {
    @Override
    protected Void doInBackground(AsyncModel... asyncModels) {
        AsyncModel model = asyncModels[0];
        Ingredient ingredient = (Ingredient) model.getModel();
        RestfulUtil.getInstance().postData(RestfulUtil.SAVE_INGREDIENT +ingredient.getId() + "/",
                ingredient.toJson());
        return null;
    }
}
