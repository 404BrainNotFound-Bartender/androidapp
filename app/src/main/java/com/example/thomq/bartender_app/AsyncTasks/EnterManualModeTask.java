package com.example.thomq.bartender_app.AsyncTasks;

import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.thomq.bartender_app.Models.AsyncModel;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Services.RestfulUtil;
import com.example.thomq.bartender_app.Views.ManualMode;

import org.json.JSONException;
import org.json.JSONObject;

public class EnterManualModeTask extends AsyncTask<AsyncModel, Void, Integer> {
    private AsyncModel model;
    @Override
    protected Integer doInBackground(AsyncModel... models) {
         model = models[0];
        JSONObject object = RestfulUtil.getInstance().getData(RestfulUtil.ENTER_MANUAL_MODE);
        try {
            if(object.getBoolean("entered")){
                DataService.getInstance().setManualModeId(object.getString("manualId"));
                return null;
            }
            else return object.getInt("queue");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        if(integer == null){
            model.getContext().startActivity(new Intent(model.getContext(), ManualMode.class));
        }else if(integer == -1){
            Toast.makeText(model.getContext(),
                    "Failed to enter mode", Toast.LENGTH_LONG).show();
        }else Toast.makeText(model.getContext(),
                "Please wait there is " + integer.toString() + " drinks in queue",
                Toast.LENGTH_LONG).show();
    }
}
