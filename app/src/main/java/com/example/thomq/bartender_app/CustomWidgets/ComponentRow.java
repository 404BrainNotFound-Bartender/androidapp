package com.example.thomq.bartender_app.CustomWidgets;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.thomq.bartender_app.ArrayAdapters.IngredientArrayAdapter;
import com.example.thomq.bartender_app.CustomPopupWindows.AddRecipePopup;
import com.example.thomq.bartender_app.CustomWidgets.Ingredients.IngredientButton;
import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Models.Ingredient;
import com.example.thomq.bartender_app.Services.DecimalDigitsInputFilter;

import java.util.ArrayList;
import java.util.List;

public class ComponentRow extends TableRow {
    static private Ingredient newIngredient = new Ingredient();
    static {
        newIngredient.setName("Add New Ingredient");
    }
    Spinner ingredientSpinner;
    Ingredient ingredient;
    List<Ingredient> ingredientList = new ArrayList<>();
    TableLayout tableLayout;
    Context context;

    EditText amountTxt;
    double amount;

    Spinner unitSpinner;
    int unit;

    FloatingActionButton removeBtn;

    public ComponentRow(Context context, TableLayout tableLayout, Double amount, Ingredient ingredient){
        this(context, tableLayout);

        int pos;
        for(pos =0; pos < ingredientSpinner.getAdapter().getCount(); pos++){
            if(ingredientSpinner.getAdapter().getItem(pos).equals(ingredient.getName()+"-"+ingredient.getProof()))
                break;
        }
        ingredientSpinner.setSelection(pos);
        amountTxt.setText(amount.toString());
    }

    public ComponentRow(Context context, TableLayout tableLayout) {
        super(context);
        this.context = context;
        this.tableLayout = tableLayout;
        ingredientList.add(newIngredient);
        ingredientList.addAll(DataService.getInstance().getIngredients());
        setGravity(Gravity.CENTER);
        ingredientSpinner = new Spinner(context);
        ingredientSpinner.setMinimumWidth(300);
        ingredientSpinner.setAdapter(
                new IngredientArrayAdapter(context,
                android.R.layout.simple_dropdown_item_1line,
                ingredientList));
        ingredientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ingredient = ingredientList.get(position);
                if(ingredient == newIngredient){
                    AddRecipePopup popup = new AddRecipePopup(context);
                    popup.setOnDismissListener(() -> {
                        if(popup.getIngredient() != null) ingredient = popup.getIngredient();
                        ingredientList.add(ingredient);
                        ingredientSpinner.setAdapter(
                                new IngredientArrayAdapter(context,
                                        android.R.layout.simple_dropdown_item_1line,
                                        ingredientList));
                        ingredientSpinner.setSelection(ingredientList.indexOf(ingredient));
                    });
                    popup.show(tableLayout);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ingredientSpinner.setSelection(1);


        amountTxt = new EditText(context);
        amountTxt.setWidth(200);
        amountTxt.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        amountTxt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(5, 2)});
        unitSpinner = new Spinner(context);
        unitSpinner.setAdapter(new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_dropdown_item, new String[]{"oz", "cup", "ml"}));

        IngredientButton ingredientButton = new IngredientButton(context);
        addView(ingredientButton);
//        addView(ingredientSpinner);
        addView(amountTxt);
        addView(unitSpinner);

        removeBtn = new FloatingActionButton(context);
        removeBtn.setOnClickListener(view-> tableLayout.removeView(this));
        removeBtn.setImageResource(android.R.drawable.ic_delete);
        removeBtn.setBackgroundColor(Color.TRANSPARENT);
        removeBtn.setSize(4);
        removeBtn.setCustomSize(50);
        addView(removeBtn);
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public double getAmount() {
        if(amountTxt.getText() == null || amountTxt.getText().toString().equals(""))return 0;
        switch (unitSpinner.getSelectedItemPosition()){
            case 0:
                amount = Double.parseDouble(amountTxt.getText().toString());
                break;
            case 1:
                amount = Double.parseDouble(amountTxt.getText().toString()) * 8;
                break;
            case  2:
                amount = Double.parseDouble(amountTxt.getText().toString()) * 0.033814;
                break;
        }
        amount *= ingredient.getDensity();
        return amount;
    }

    public EditText getAmountTxt() {
        return amountTxt;
    }

    public boolean isFilledOut() {
        return ingredientSpinner.getSelectedItem() != null && amountTxt.getText() != null && !amountTxt.getText().toString().equals("") && unitSpinner.getSelectedItem() != null;
    }
}