package com.example.thomq.bartender_app.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class Pump implements ModelBase{
    private String id = "";
    private Integer pin;
    private String name;

    public Pump(){}

    public Pump(JSONObject object){
        try {
            id = object.getString("id");
            pin = object.getInt("pin");
            name = object.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject toJson(){
        try {
            JSONObject object = new JSONObject();
            object.put("name", name);
            object.put("pin", pin);
            return object;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
