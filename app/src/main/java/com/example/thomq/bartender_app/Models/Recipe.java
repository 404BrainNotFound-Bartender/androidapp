package com.example.thomq.bartender_app.Models;


import com.example.thomq.bartender_app.CustomWidgets.ComponentRow;
import com.example.thomq.bartender_app.Services.DataService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Recipe implements ModelBase{
    private String name;
    private String id;
    private Map<Ingredient, Double> ingredients = new HashMap<>();
    private boolean canMake;
    private double servingSize;
    private double rating;

    public Recipe(){}

    public Recipe(JSONObject jsonObject) throws JSONException {
        this(jsonObject.getString("name"), jsonObject.getString("id"),
                jsonObject.getBoolean("canMake"), jsonObject.getDouble("serving"),
                jsonObject.getJSONObject("parts"), jsonObject.getDouble("rating"));
    }

    public Recipe(String name, String id, boolean canMake, double serveringSize,
                  JSONObject parts, double rating) {
        this.name = name;
        this.id = id;
        this.canMake = canMake;
        this.servingSize = serveringSize;
        Iterator<String> keys = parts.keys();
        while (keys.hasNext()){
            String index = keys.next();
            try {
                String ingre  = parts.getJSONObject(index).getString("ingredient");
                for(Ingredient ingredient: DataService.getInstance().getIngredients()){
                    if(ingredient.getId().equals(ingre)){
                        ingredients.put(ingredient, parts.getJSONObject(index).getDouble("amount"));
                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        this.rating = rating;
    }

    public void saveComponents(ComponentRow[] componentRows){
        double mass = 0.0;
        for(ComponentRow row: componentRows) mass += row.getAmount();
        servingSize = mass;
        ingredients.clear();
        for(ComponentRow row: componentRows)ingredients.put(row.getIngredient(),
                row.getAmount() / servingSize);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<Ingredient, Double> getIngredients() {
        return ingredients;
    }

    public boolean isCanMake() {
        return canMake;
    }

    public void setCanMake(boolean canMake) {
        this.canMake = canMake;
    }

    public double getServingSize() {
        return servingSize;
    }

    public void setServingSize(double servingSize) {
        this.servingSize = servingSize;
    }

    public JSONObject toJson(){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", id);
            jsonObject.put("name", name);
            jsonObject.put("serving", servingSize);
            JSONArray jsonArray = new JSONArray();
            for(Map.Entry<Ingredient, Double> entry: ingredients.entrySet()){
                JSONObject ingre = new JSONObject();
                ingre.put("amount", entry.getValue());
                ingre.put("id", entry.getKey().getId());
                jsonArray.put(ingre);
            }
            jsonObject.put("components", jsonArray);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setIngredients(Map<Ingredient, Double> ingredients) {
        this.ingredients = ingredients;
    }

    public double getRating() {
        return rating;
    }
}
