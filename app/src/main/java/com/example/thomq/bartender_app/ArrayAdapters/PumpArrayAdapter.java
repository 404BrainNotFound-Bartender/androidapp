package com.example.thomq.bartender_app.ArrayAdapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.thomq.bartender_app.Models.Pump;

import java.util.List;

public class PumpArrayAdapter extends ArrayAdapter<Pump> {

    private List<Pump> pumps;
    public PumpArrayAdapter(@NonNull Context context, int resource, @NonNull List<Pump> objects) {
        super(context, resource, objects);
        this.pumps = objects;
    }

    @Override
    public int getCount() {
        return pumps.size();
    }


    @Nullable
    @Override
    public Pump getItem(int position) {
        return pumps.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(pumps.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(pumps.get(position).getName());
        return label;
    }
}
