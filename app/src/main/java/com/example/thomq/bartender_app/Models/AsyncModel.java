package com.example.thomq.bartender_app.Models;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;

import com.example.thomq.bartender_app.CustomPopupWindows.ProgressDialogPopup;
import com.example.thomq.bartender_app.Views.MainActivity;

public class AsyncModel {
    private Context context;
    private Object item;
    private ProgressDialog dialog;
    private View anchor;
    private ProgressDialogPopup progressDialogPopup;

    public AsyncModel(Context context, Object item){
        this.context = context;
        this.item = item;
    }

    public AsyncModel(Context context, Object item, View anchor){
        this(context, item);
        this.anchor = anchor;
    }

    public AsyncModel(Context context, Object item, ProgressDialog dialog){
        this(context, item);
        this.dialog = dialog;
    }

    public AsyncModel(Context context, Object item, ProgressDialogPopup progressDialogPopup){
        this(context, item);
        this.progressDialogPopup = progressDialogPopup;
    }

    public AsyncModel(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public Object getItem() {
        return item;
    }

    public Double getDouble(){
        return (Double) item;
    }

    public Integer getInteger(){
        return (Integer) item;
    }

    public Boolean getBoolean(){
        return (Boolean) item;
    }

    public String getString(){
        return (String) item;
    }

    public ModelBase getModel(){
        return (ModelBase) item;
    }

    public ProgressDialog getDialog() {
        return dialog;
    }

    public View getPopupAnchor(){
        return anchor;
    }

    public ProgressDialogPopup getProgressDialogPopup() {
        return progressDialogPopup;
    }
}
