package com.example.thomq.bartender_app.CustomWidgets;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.thomq.bartender_app.Services.DataService;
import com.example.thomq.bartender_app.Models.Ingredient;

public class AlternativeRow extends TableRow {

    Ingredient selectedIngredient;
    Spinner spinner;


    public AlternativeRow(Context context, TableLayout tableLayout) {
        super(context);
        spinner = new Spinner(context);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item);
        for(Ingredient ingredient: DataService.getInstance().getIngredients()) adapter.add(ingredient.getName() + "-" + ingredient.getProof());
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedIngredient = DataService.getInstance().getIngredients().get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addView(spinner);

        FloatingActionButton removeBtn = new FloatingActionButton(context);
        removeBtn.setOnClickListener(view-> tableLayout.removeView(this));
        removeBtn.setImageResource(android.R.drawable.ic_delete);
        removeBtn.setBackgroundColor(Color.TRANSPARENT);
        removeBtn.setSize(4);
        removeBtn.setCustomSize(50);
        addView(removeBtn);
    }


    public Ingredient getSelectedIngredient() {
        return selectedIngredient;
    }

    public void setSelectedIngredient(Ingredient selectedIngredient) {
        this.selectedIngredient = selectedIngredient;
        spinner.setSelection(DataService.getInstance().getIngredients().indexOf(selectedIngredient));
    }
}
