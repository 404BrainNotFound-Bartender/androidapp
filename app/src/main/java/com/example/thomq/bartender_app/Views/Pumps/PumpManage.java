package com.example.thomq.bartender_app.Views.Pumps;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;

import com.example.thomq.bartender_app.CustomWidgets.PumpRow;
import com.example.thomq.bartender_app.R;
import com.example.thomq.bartender_app.Services.DataService;

public class PumpManage extends AppCompatActivity {

    TableLayout tableLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pump_manage);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ProgressDialog progressDialog = new ProgressDialog(this);


        tableLayout = findViewById(R.id.pumpManageTable);
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                progressDialog.setMessage("Get Pumps");
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                DataService.getInstance().getPumps();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                DataService.getInstance().getPumps().stream()
                        .filter(pump -> pump != DataService.getNoPump())
                        .map(pump -> new PumpRow(PumpManage.this, pump))
                        .forEach(tableLayout::addView);
                progressDialog.dismiss();
            }
        };
        task.execute();


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> tableLayout.addView(new PumpRow(PumpManage.this)));

    }

}
